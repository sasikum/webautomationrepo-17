package com.tatahealth.EMR.pages.Billing;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class MasterAdministration {
	public WebElement getEmrMasterAdminTab(WebDriver driver,ExtentTest logger,String tab) 
	{
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//ul[@id='masterAmin']/li/a[contains(.,'"+tab+"')]", driver, logger);
		return element;
}
	public WebElement getEmrMasterAdminConfigMasterSearchTextBox(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//div[@id='ConfigurationMasterstable_filter']/label/input", driver, logger);
		return element;
		
}
	public WebElement getEmrMasterAdminConfigMasterSearchEdit(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//table[@id='ConfigurationMasterstable']/tbody/tr/td/a", driver, logger);
		return element;
		
}
	public void getEmrMasterAdminConfigMasterBillTypeDropdown(WebDriver driver,ExtentTest logger,String billType) throws Exception 
	{
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//select[@id='paramValue']", driver, logger);
		Web_GeneralFunctions.selectElement(element,billType,"Bill TYpe", driver, logger);
		
}
	public WebElement getEmrMasterAdminConfigMasterSaveButton(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//div[@id='systemParamsMaster']//button[contains(.,'Save')]", driver, logger);
		return element;
	}
		
}
